from django.urls import path
from .views import create_task, show_my_tasks, list_tasks

urlpatterns = [
    path("create/", create_task, name="create_task"),
    path("mine/", show_my_tasks, name="show_my_tasks"),
    path("", list_tasks, name="list_tasks"),
]
